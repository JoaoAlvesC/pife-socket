from baralho import Baralho

class Game:
    def __init__(self, server):
        self.server = server
        self.should_restart = True
        self.players = server.players
        self.baralho = Baralho()
        self.dead = []
        self.state = 0
        self.winner = None

    def hand_out_cards(self):
        for j in range(9):
            for player in self.players:
                player.pick_card(self.baralho.cartas.pop(0))

    def start(self):
        while self.state != 2:
            round_res = self.play_round()
            if round_res == 2:
                break

        self.server.broadcast_message("-*-* GAME OVER *-*-")

        if self.winner is not None:
            self.server.broadcast_message(f"O vencedor é {self.winner.name}! Parabéns")
            self.server.broadcast_message(f"{self.winner.cartas_down_to_string()}")

        for player in self.players:
            resp = player.ask_message("Deseja jogar uma nova partida? (s/n) \n# ")
            if resp.lower() != "s":
                self.should_restart = False
                break

    def reset(self):
        self.baralho.reset()
        self.state = 0
        self.winner = None
        for player in self.players:
            player.reset()
        self.hand_out_cartas()

    def play_round(self):
        for player in self.players:
            if self.state == 2:
                return 2
            self.server.broadcast_message(f"{player.name} jogando...")
            player.play(self)

    def grab_from_deck(self):
        last_card = self.baralho.cartas.pop()
        return last_card

    def grab_from_dead(self):
        last_card = self.dead.pop()
        return last_card

    def print(self):
        for player in self.players:
            print(player.to_string())
