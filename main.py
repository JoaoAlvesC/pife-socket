from _socket import SHUT_RDWR
from threading import Thread
from server import Server
from client import Client
from game import Game
from constants import MAX_PLAYERS

def main():
    port = 5050
    while True:
        server = Server(port)
        server.start()

        if server.isRunning:
            client = Client(port)
            client.connect()
            conn_result = client.start()

            if conn_result == -1:
                client.sock.close()
                del client
                port += 4
            else:
                del server
                break

        else:
            Thread(target=server.run_loop).start()

            last_len = None

            while True:
                if len(server.players) == MAX_PLAYERS:
                    server.broadcast_message("...")
                    break
                elif len(server.players) != last_len:
                    last_len = len(server.players)
                    print(f"Jogadores ({len(server.players)}/{MAX_PLAYERS})")

            game = Game(server)
            game.baralho.shuffle()
            game.hand_out_cards()

            # loop para quando um jogo terminar ja comecar outro a menos que
            while game.should_restart:
                game.start()
                game.reset()

        server.sock.shutdown(SHUT_RDWR)
        server.sock.close()
        exit(1)

main()
