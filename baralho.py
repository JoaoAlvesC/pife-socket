from random import shuffle
from carta import Carta

class Baralho:
    def __init__(self):
        self.cartas = []
        self.reset()

    def reset(self):
        for i in range(4):
            for j in range(13):
                temp = Carta(i, j)
                self.cartas.append(temp)
                self.cartas.append(temp)

    def shuffle(self):
        shuffle(self.cartas)
