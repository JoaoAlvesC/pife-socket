elementos = [
    ['0 A ', '0 2', '0 3', '0 4', '0 5', '0 6', '0 7', '0 8', '0 9', '0 10', '0 J', '0 Q', '0 K'],
    ['1 A', '1 2', '1 3', '1 4', '1 5', '1 6', '1 7', '1 8', '1 9', '1 10', '1 J', '1 Q', '1 K'],
    ['2 A', '2 2', '2 3', '2 4', '2 5', '2 6', '2 7', '2 8', '2 9', '2 10', '2 J', '2 Q', '2 K'],
    ['3 A', '3 2', '3 3', '3 4', '3 5', '3 6', '3 7', '3 8', '3 9', '3 10', '3 J', '3 Q', '3 K']
]

class Carta:
    def __init__(self, naipe, valor):
        self.naipe = naipe
        self.valor = valor

    def to_string(self):
        return f"{elementos[self.naipe][self.valor]} |"
