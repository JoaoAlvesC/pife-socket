class Sock:
    @classmethod
    def send_message(cls, sock, message, code="002"):
        formatted_msg = "\n" + code + "::" + message + "<br>"
        sock.send(formatted_msg.encode())
